﻿pm_anchorage = {
	texture = "gfx/interface/icons/production_method_icons/anchorage.dds"
	
	low_pop_method = yes # AI will activate this method for states with low population
	
	building_modifiers = {
		workforce_scaled = {
			building_employment_laborers_add = 500
			building_employment_clerks_add = 350
			building_employment_bureaucrats_add = 150
		}
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_convoys_capacity_add = 20
		}
	}
}			

pm_basic_port = {
	texture = "gfx/interface/icons/production_method_icons/basic_port.dds"
	
	building_modifiers = {
		workforce_scaled = {	
			building_input_clippers_add = 5
		}
		level_scaled = {
			building_employment_laborers_add = 3000
			building_employment_clerks_add = 1500
			building_employment_bureaucrats_add = 500
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_convoys_capacity_add = 150
		}
	}
	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 5
		}
	}
}		
			
