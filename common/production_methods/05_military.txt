﻿pm_no_organization = {
	texture = "gfx/interface/icons/production_method_icons/no_specialists.dds"
	is_default = yes
	building_modifiers = {

		level_scaled = {
			building_employment_soldiers_add = 970
			building_employment_officers_add = 30
			building_training_rate_add = 10
		}
	}
}

pm_general_training = {
	texture = "gfx/interface/icons/production_method_icons/cavalry.dds"
	is_default = yes
	building_modifiers = {

		level_scaled = {
			building_employment_soldiers_add = 950
			building_employment_officers_add = 50
			building_training_rate_add = 18
		}
	}
}

pm_no_organization_conscription = {
	texture = "gfx/interface/icons/production_method_icons/no_specialists.dds"
	is_default = yes
	building_modifiers = {
		level_scaled = {
			building_employment_soldiers_add = 970
			building_employment_officers_add = 30
			building_training_rate_add = 10
		}
	}
}

pm_general_training_conscription = {
	texture = "gfx/interface/icons/production_method_icons/cavalry.dds"
	is_default = yes
	building_modifiers = {
		level_scaled = {
			building_employment_soldiers_add = 950
			building_employment_officers_add = 50
			building_training_rate_add = 18
		}
	}
}

pm_no_naval_theory = {
	texture = "gfx/interface/icons/production_method_icons/no_naval_theory.dds"
	building_modifiers = {
		level_scaled = {
			building_employment_soldiers_add = 900
			building_employment_officers_add = 100
			building_training_rate_add = 3
		}
	}
}