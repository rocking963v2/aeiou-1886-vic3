﻿pm_simple_forestry = {
	texture = "gfx/interface/icons/production_method_icons/simple_forestry.dds"

	building_modifiers = {
		workforce_scaled = {
			building_output_wood_add = 30
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 4500
		}
	}
}

pm_no_hardwood = {
	texture = "gfx/interface/icons/production_method_icons/no_hardwood_selection.dds"
}

pm_hardwood = {
	texture = "gfx/interface/icons/production_method_icons/hardwood_selection.dds"
	building_modifiers = {
		workforce_scaled = {
			# output goods										
			building_output_wood_add = -20			
			building_output_hardwood_add = 10	
		}
	}
}

pm_no_equipment = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

pm_merchant_guilds_building_logging_camp = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_simple_forestry
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}

pm_privately_owned_building_logging_camp = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {
		pm_saw_mills
		pm_electric_saw_mills
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

default_building_rubber_plantation = {
	texture = "gfx/interface/icons/production_method_icons/plantation_production.dds"

	building_modifiers = {
		workforce_scaled = {
			building_output_rubber_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_farmers_add = 800
			building_employment_clergymen_add = 100
		}
	}
}

pm_simple_fishing = {
	texture = "gfx/interface/icons/production_method_icons/simple_fishing.dds"
	
	building_modifiers = {
		workforce_scaled = {
			# output
			building_output_fish_add = 25
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 4500
		}
	}
}

pm_fishing_trawlers = {
	texture = "gfx/interface/icons/production_method_icons/trawlers.dds"
	
	building_modifiers = {
		workforce_scaled = {
			# input
			building_input_clippers_add = 5
			
			# output
			building_output_fish_add = 50
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}

pm_merchant_guilds_building_fishing_wharf = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_simple_fishing
		pm_fishing_trawlers
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}

pm_privately_owned_building_fishing_wharf = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_production_methods = {				
		pm_fishing_trawlers
		pm_steam_trawlers
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}

pm_simple_whaling = {
	texture = "gfx/interface/icons/production_method_icons/simple_whaling.dds"
	
	building_modifiers = {
		workforce_scaled = {
			# input 
			
			# output
			building_output_meat_add = 5
			building_output_oil_add = 10
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 4500
		}
	}
}

pm_wooden_whaling_ships = {
	texture = "gfx/interface/icons/production_method_icons/wooden_whaling_ships.dds"
	
	building_modifiers = {
		workforce_scaled = {
			# input
			building_input_clippers_add = 5 
			
			# output
			building_output_meat_add = 10
			building_output_oil_add = 20
		}

		level_scaled = {
			# employment
			building_employment_laborers_add = 4000
			building_employment_machinists_add = 500
		}
	}
}

pm_merchant_guilds_building_whaling_station = {
	texture = "gfx/interface/icons/production_method_icons/merchant_guilds.dds"

	unlocking_production_methods = {
		pm_simple_whaling
		pm_wooden_whaling_ships
	}

	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_shopkeepers_add = 500
		}
		unscaled = {
			building_shopkeepers_shares_add = 10
		}
	}
}		

pm_privately_owned_building_whaling_station = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"
	
	unlocking_production_methods = {
		pm_wooden_whaling_ships
		pm_steam_whaling_ships
	}				
	
	disallowing_laws = {
		law_command_economy
		law_cooperative_ownership
	}

	building_modifiers = {
		level_scaled = {
			building_employment_capitalists_add = 100
		}
		unscaled = {
			building_capitalists_shares_add = 10
		}
	}
}