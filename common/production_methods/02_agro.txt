﻿pm_simple_farming = {
	texture = "gfx/interface/icons/production_method_icons/simple_farming.dds"
	building_modifiers = {
		workforce_scaled = {
			# output goods													
			building_output_grain_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_farmers_add = 600
			building_employment_clergymen_add = 300
		}
	}
}

pm_no_secondary = {
	texture = "gfx/interface/icons/production_method_icons/no_orchards.dds"
}

pm_potatoes = {
	texture = "gfx/interface/icons/production_method_icons/potatoes.dds"
	
	building_modifiers = {
		workforce_scaled = {
			building_output_grain_add = -20
			building_output_liquor_add = 15
		}
	}
}

pm_apple_orchards = {
	texture = "gfx/interface/icons/production_method_icons/orchards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			building_output_grain_add = -20
			building_output_fruit_add = 10
			building_output_sugar_add = 5
		}
	}
}

pm_tools_disabled = {
	texture = "gfx/interface/icons/production_method_icons/no_tool_use.dds"
}

pm_tools = {
	texture = "gfx/interface/icons/production_method_icons/harvesting_tools.dds"
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			building_input_tools_add = 1
		}

		level_scaled = {
			building_employment_laborers_add = -500
		}
	}
}

pm_privately_owned = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds"

	unlocking_laws = {
		law_serfdom
		law_tenant_farmers
	}

	building_modifiers = {
		level_scaled = {
			building_employment_aristocrats_add = 100
		}
		unscaled = {
			building_aristocrats_shares_add = 10 
			building_clergymen_shares_add = 2
			building_farmers_shares_add = 0.25
		}
	}
}

pm_homesteading = {
	texture = "gfx/interface/icons/production_method_icons/homesteading.dds" 

	unlocking_laws = {
		law_homesteading
	}

	building_modifiers = {
		level_scaled = {
			building_employment_aristocrats_add = 50
			building_employment_farmers_add = 50
		}
		unscaled = {
			building_aristocrats_shares_add = 10
			building_clergymen_shares_add = 1
			building_farmers_shares_add = 2
		}
	}
}

pm_citrus_orchards = {
	texture = "gfx/interface/icons/production_method_icons/orchards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			building_output_grain_add = -20
			building_output_fruit_add = 9
			building_output_sugar_add = 6
		}
	}
}

pm_vineyards = {
	texture = "gfx/interface/icons/production_method_icons/vineyards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			building_output_grain_add = -15
			building_output_wine_add = 8
		}
		
		level_scaled = {
			building_employment_shopkeepers_add = 200
		}
	}
}

pm_simple_farming_building_rice_farm = {
	texture = "gfx/interface/icons/production_method_icons/simple_farming.dds"
	building_modifiers = {
		workforce_scaled = {
			# output goods
			building_output_grain_add = 35
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_farmers_add = 600
			building_employment_clergymen_add = 300
		}
	}
}

pm_fig_orchards = {
	texture = "gfx/interface/icons/production_method_icons/orchards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			building_output_grain_add = -15
			building_output_fruit_add = 6
			building_output_sugar_add = 9
		}
	}
}						

pm_vineyards_building_maize_farm = {
	texture = "gfx/interface/icons/production_method_icons/vineyards.dds"
	
	building_modifiers = {
		workforce_scaled = {
			building_output_grain_add = -12
			building_output_wine_add = 6
		}

		level_scaled = {
			building_employment_shopkeepers_add = 150
		}
	}
}

pm_simple_ranch = {
	texture = "gfx/interface/icons/production_method_icons/simple_ranching.dds"

	building_modifiers = {
		workforce_scaled = {
			building_output_fabric_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_farmers_add = 700
			building_employment_clergymen_add = 200
		}
	}
}

pm_open_air_stockyards = {
	texture = "gfx/interface/icons/production_method_icons/open_air_stockyards.dds"		
	building_modifiers = {
		workforce_scaled = {
			building_output_meat_add = 5
		}
	}
}		

pm_butchering_tools = {
	texture = "gfx/interface/icons/production_method_icons/butchering_tools.dds"			
	building_modifiers = {				
		workforce_scaled = {
			building_input_tools_add = 5
			building_output_meat_add = 15
		}
	}							
}

pm_standard_fences = {
	texture = "gfx/interface/icons/production_method_icons/standard_fences.dds"
}


pm_unrefrigerated = {
	texture = "gfx/interface/icons/production_method_icons/no_refrigeration.dds"

}


### AEIOU ###

pm_no_specialization = {
	texture = "gfx/interface/icons/production_method_icons/simple_ranching.dds"

	building_modifiers = {
		workforce_scaled = {
			building_output_fabric_add = 5
			building_output_meat_add = 2
			building_output_workhorse_add = 15
			building_output_racehorse_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_farmers_add = 700
			building_employment_clergymen_add = 200
		}
	}
}

pm_donkey_crossbreeding = {
	texture = "gfx/interface/icons/production_method_icons/simple_ranching.dds"

	building_modifiers = {
		workforce_scaled = {
			building_output_fabric_add = 6
			building_output_meat_add = 3
			building_output_workhorse_add = 25
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_farmers_add = 700
			building_employment_clergymen_add = 200
		}
	}
}

pm_artificial_selection = {
	texture = "gfx/interface/icons/production_method_icons/simple_ranching.dds"

	building_modifiers = {
		workforce_scaled = {
			building_output_fabric_add = 4
			building_output_meat_add = 1
			building_output_racehorse_add = 15
		}

		level_scaled = {
			building_employment_laborers_add = 4000
			building_employment_farmers_add = 700
			building_employment_clergymen_add = 200
		}
	}
}

pm_normal_function = {
	texture = "gfx/interface/icons/production_method_icons/homesteading.dds" 

	building_modifiers = {
		unscaled = {
			building_output_fabric_mult = -1
			building_output_meat_mult = -1
		}
	}
}

pm_horses_for_meat = {
	texture = "gfx/interface/icons/production_method_icons/homesteading.dds" 

	building_modifiers = {
		unscaled = {
			building_output_workhorse_mult = -1
			building_output_racehorse_mult = -1
		}
	}
}

pm_free_roam = {
	texture = "gfx/interface/icons/production_method_icons/standard_fences.dds"
}