﻿pmg_amenities = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_market_stalls
	}
}

pmg_street_lighting = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_street_lighting
		pm_gas_streetlights
	}
}

pmg_public_transport = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_public_transport
	}
}

pmg_urban_clergy = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_state_urban_clergy
	}
}