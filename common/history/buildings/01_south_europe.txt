﻿BUILDINGS={
	s:STATE_LOMBARDY={
		region_state:LOM={
		}
		region_state:VEN={
		}
		region_state:MAN={		
		}
	}
	s:STATE_PIEDMONT={
		region_state:SAV={
		}
		region_state:GEN={
		}
		region_state:MNT={
		}
	}
	s:STATE_SARDINIA={
		region_state:ARB={
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_fishing_wharf" "pm_unrefrigerated" "pm_simple_fishing" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_anchorage" }
			}
			create_building={
				building="building_galena_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_galena_mine" "pm_merchant_guilds_building_galena_mine" }
			}
			create_building={
				building="building_horse_stable"
				level=1
				reserves=1
				activate_production_methods={ "pm_no_specialization" "pm_normal_function" "pm_standard_fences" "pm_free_roam" "pmg_ownership_land_building_horse_stable"}
			}
		}
		region_state:GEN={
		}
		region_state:TUS={
		}
	}
	s:STATE_MALTA={
		region_state:SIC={
		}
	}
	s:STATE_UMBRIA={
		region_state:PAP={
		}
	}
	s:STATE_CORSICA={
		region_state:GEN={
		}
	}
	s:STATE_CAMPANIA={ 
		region_state:NAP={
		}
		region_state:PAP={
		}
	}
	s:STATE_CALABRIA={
		region_state:NAP={		
		}
	}
	s:STATE_APULIA={
		region_state:NAP={
		}
	}
	s:STATE_ABRUZZO={
		region_state:NAP={
		}
	}
	s:STATE_SAVOY={
		region_state:SAV={
		}
	}
	s:STATE_EMILIA={
		region_state:LOM={
		}
		region_state:LUC={			
		}
		region_state:MOD={
		}
	}
	s:STATE_TUSCANY={
		region_state:TUS={
		}
	}
	s:STATE_ROMAGNA={
		region_state:PAP={
		}
		region_state:VEN={
		}
		region_state:FRR={
		}
	}
	s:STATE_LAZIO={
		region_state:PAP={
		}
	}
	s:STATE_VENETIA={
		region_state:VEN={
		}
	}
	s:STATE_SICILY={
		region_state:SIC={
		}
	}
	s:STATE_TOLEDO={
		region_state:SPA={
		}
	}
	s:STATE_GALICIA={
		region_state:SPA={
		}
	}
	s:STATE_ASTURIAS={
		region_state:SPA={
		}
	}
	s:STATE_CASTILE={
		region_state:SPA={
		}
	}
	s:STATE_NAVARRA={
		region_state:SPA={
		}
	}
	s:STATE_ARAGON={
		region_state:SPA={
		}
	}
	s:STATE_CATALONIA={
		region_state:SPA={
		}
	}
	s:STATE_BALEARES={
		region_state:SPA={
		}
	}
	s:STATE_ANDALUSIA={
		region_state:SPA={
		}
	}
	s:STATE_GRANADA={
		region_state:SPA={
		}
	}
	s:STATE_BADAJOZ={
		region_state:SPA={
		}
	}
	s:STATE_VALENCIA={
		region_state:SPA={
		}
	}
	s:STATE_BEIRA={
		region_state:POR={
		}
	}
	s:STATE_ESTREMADURA={
		region_state:POR={
		}
	}
	s:STATE_ALENTEJO={
		region_state:POR={
		}
	}
	s:STATE_CAPE_VERDE={
		region_state:POR={
		}
	}
	s:STATE_CANARY_ISLANDS={
		region_state:SPA={
		}
	}
	s:STATE_AZORES={
		region_state:POR={
		}
	}
	s:STATE_MADEIRA={
		region_state:POR={
		}
	}
	s:STATE_EASTERN_THRACE={
		region_state:GRE={
		}
	}
	s:STATE_DOBRUDJA={
		region_state:BUL={
		}
	}
	s:STATE_ALBANIA={
		region_state:ALB={
		}
	}
	s:STATE_BOSNIA={
		region_state:AUS={
		}
	}
	s:STATE_NORTHERN_THRACE={
		region_state:BUL={
		}
	}
	s:STATE_BULGARIA={
		region_state:BUL={
		}
	}
	s:STATE_SLOVENIA={
		region_state:AUS={
		}
	}
	s:STATE_DALMATIA={
		region_state:VEN={
		}
	}
	s:STATE_CROATIA={
		region_state:AUS={
		}
		region_state:VEN={
		}
	}
	s:STATE_SLAVONIA={
		region_state:AUS={
		}
	}
	s:STATE_ISTRIA={
		region_state:AUS={
		}
		region_state:VEN={
		}
	}
	s:STATE_MONTENEGRO={
		region_state:SER={
		}
	}
	s:STATE_NORTHERN_SERBIA={
		region_state:SER={
		}
	}
	s:STATE_SOUTHERN_SERBIA={
		region_state:SER={
		}
	}
	s:STATE_CRETE={
		region_state:VEN={
		}
	}
	s:STATE_WEST_AEGEAN_ISLANDS={
		region_state:GRE={
		}
	}
	s:STATE_EAST_AEGEAN_ISLANDS={
		region_state:GRE={
		}
	}
	s:STATE_THESSALIA={
		region_state:GRE={
		}
	}
	s:STATE_SKOPIA={
		region_state:GRE={
		}
	}
	s:STATE_MACEDONIA={
		region_state:GRE={
		}
	}
	s:STATE_WESTERN_THRACE={
		region_state:GRE={
		}
	}
	s:STATE_IONIAN_ISLANDS={
		region_state:VEN={
		}
	}
	s:STATE_ATTICA={
		region_state:GRE={
		}
	}	
	s:STATE_PELOPONNESE={
		region_state:GRE={	
		}
	}
	
	s:STATE_CANARY_ISLANDS={
		region_state:UNF={
		}
	}
	
	s:STATE_MADEIRA={
		region_state:UNF={
		}
	}
	
	s:STATE_AZORES={
		region_state:UNF={
		}
	}
	
	s:STATE_CAPE_VERDE={
		region_state:UNF={
		}
	}


}




































































































