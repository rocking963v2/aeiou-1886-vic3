﻿COUNTRIES = {
	c:NET = {
		effect_starting_technology_advanced_tier = yes
		effect_starting_politics_traditional = yes

		ig:ig_petty_bourgeoisie = {
			add_ruling_interest_group = yes
		}

		ig:ig_devout = {
			add_ruling_interest_group = yes
		}
	}
}
