﻿COUNTRIES = {
	c:PLC = {
		effect_starting_technology_advanced_tier = yes
		effect_starting_politics_traditional = yes

		ig:ig_devout = {
			set_interest_group_name = ig_catholic_church
		}
	}
}