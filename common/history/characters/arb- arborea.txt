﻿CHARACTERS = {
	c:ARB = {
		create_character = {
			# Marianus IX
			first_name = "marianus_ix"
			last_name = "de_arbaree"
			ruler = yes
			age = 27
			dna = DNA_marianus_ix
			interest_group = ig_landowners
			ideology = ideology_traditionalist
			traits = {
				basic_political_operator
				imperious
			}
			on_created = {
				set_variable = is_married
				set_variable = marianus_ix
			}
		}	
	}
}
