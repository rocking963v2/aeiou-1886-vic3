﻿CHARACTERS = {
	c:NAP = {
		create_character = {
			# Francesco II
			first_name = "ferrante_ix"
			last_name = "d_trastamara"
			ruler = yes
			age = 45
			dna = DNA_francesco_ii
			interest_group = ig_intelligentsia
			ideology = ideology_moderate
			traits = {
				ambitious
			}
			on_created = {
				set_variable = is_married
			}
		}	
	}
}
