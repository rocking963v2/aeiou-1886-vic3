﻿DIPLOMACY = {
	c:AUS = {	
		create_diplomatic_pact = {
			country = c:LIP
			type = customs_union
		}			
		create_diplomatic_pact = {
			country = c:SCM
			type = customs_union
		}	
		create_diplomatic_pact = {
			country = c:SAX
			type = customs_union
		}				
		create_diplomatic_pact = {
			country = c:MEC
			type = customs_union
		}			
		create_diplomatic_pact = {
			country = c:HES
			type = customs_union
		}		
		create_diplomatic_pact = {
			country = c:NAS
			type = customs_union
		}	
		create_diplomatic_pact = {
			country = c:LUB
			type = customs_union
		}		
		create_diplomatic_pact = {
			country = c:WUR
			type = customs_union
		}	
		create_diplomatic_pact = {
			country = c:BAD
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:BAV
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:THR
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:HAM	
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:BRE
			type = customs_union
		}	
		create_diplomatic_pact = {
			country = c:OLD
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:BRA
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:BDN
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:HAN
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:HOL
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:LOR
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:WES
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:BRG
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:BEG
			type = customs_union
		}
		create_diplomatic_pact = {
			country = c:LUX
			type = customs_union
		}
	}
}